'''
Created on Mar 10, 2019

@author: MOHAMADA
'''

import unittest
import requests
from lxml import html




class TestWebSite(unittest.TestCase):
    
    
    '''
    setup method that initialize all the needed objects 
        1) define the target URL
        2) define the search strings : string1 & string2
        3) Running http GET method on with the defined URL
        4) Getting the response object
        5) check the response status code , and print a clear message in case status-code isn't ok(200)
        6) getting the web page content and store it into class field named content
    '''
    def setUp(self):
        unittest.TestCase.setUp(self)
        self.url='https://the-internet.herokuapp.com/context_menu'
        response = requests.get(self.url)
        if response.status_code is not 200 :
            print('Web site['+self.url+'] not available')
            print('Response status code='+response.status_code)
        self.content = response.content
        
        
    ''' 
    TestCase1 : test if the first search string exist into the page
        1) simple check : to test that the response html content include the string
        2) to use xpath : to test the search string exist in the correct element in response html content
    '''
    def test_testCase1(self):
        print('Running TestCase >> TC1 <<')
        searchString = "Right-click in the box below to see one called 'the-internet'"
        self.assertTrue(searchString in self.content ,'Search String ['+searchString+'] not found on page')
        htmlSource = html.fromstring(self.content)
        matches = htmlSource.xpath('//div[@class="example"]//p[2]')
        self.assertIsNotNone(matches[0] , 'Search string not found on page')
        self.assertIn(searchString, matches[0].text_content())
        
        
    ''' 
    TestCase2 : test if the second search string [Alibaba] exist into the page
        simple check : to test that the response html content include the string
    '''
    def test_testCase2(self):
        print('Running TestCase >> TC2 <<')
        searchString = "Alibaba"
        self.assertTrue(searchString in self.content,'Search String ['+searchString+'] not found on page')
        
        
if __name__ == '__main__':
    unittest.main()